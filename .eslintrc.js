module.exports = {
    env: {
      browser: true,
      es6: true
    },
    parserOptions: {
      ecmaFeatures: {
        "jsx": true,
        "experimentalObjectRestSpread": true
      },
      "ecmaVersion": 7,
      "sourceType": "module"
    },
    plugins: [
      'react',
    ],
    rules: {
    },
  };
  