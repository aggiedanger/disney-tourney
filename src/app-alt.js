import React, {useEffect} from 'react';
import NewMatchup from './components/new-matchup'
import {newInput} from './data-model';
import {useSelector, useDispatch} from 'react-redux';
import './styles.css';
import {loadGame} from './actions';

const appAlt = () => {

    const dispatch = useDispatch();
	dispatch(loadGame(newInput))

    const game = useSelector(state => state.game);
	console.log(game)

    return (
        <React.Fragment>
        	<section id="bracket">
				<div className="container">
					<div className="split split-one">
					<div className="round round-one current">
						<div className="round-details">Round 1<br/></div>
							{game.regionNew["Magic Kingdom"].keys.map(key => {
								{
									let roundIndicator = key - game.roundMeta[0].bracketIdMultiplier;
									if (roundIndicator < 100 && roundIndicator > 0) { 
										return <NewMatchup bracketId={key} />
									}
								}
							})}
							
							{game.regionNew["Animal Kingdom"].keys.map(key => {
								{
									let roundIndicator = key - game.roundMeta[0].bracketIdMultiplier;
									if (roundIndicator < 100 && roundIndicator > 0) { 
										return <NewMatchup bracketId={key} />
									}
								}
							})}
							
							
						</div>
					</div>
					<div className="round round-two current">
						<div className="round-details">Round 2<br/></div>			
						{game.regionNew["Magic Kingdom"].keys.map(key => {
								{
									let roundIndicator = key - game.roundMeta[1].bracketIdMultiplier;
									if (roundIndicator < 100 && roundIndicator > 0) { 
										return <NewMatchup bracketId={key} />
									}
								}
							})}
							
							{game.regionNew["Animal Kingdom"].keys.map(key => {
								{
									let roundIndicator = key - game.roundMeta[1].bracketIdMultiplier;
									if (roundIndicator < 100 && roundIndicator > 0) { 
										return <NewMatchup bracketId={key} />
									}
								}
							})}
													
					</div>	
					<div class="round round-three current">
						<div class="round-details">Round 3<br/></div>			
						{game.regionNew["Magic Kingdom"].keys.map(key => {
								{
									let roundIndicator = key - game.roundMeta[2].bracketIdMultiplier;
									if (roundIndicator < 100 && roundIndicator > 0) { 
										return <NewMatchup bracketId={key} />
									}
								}
							})}
							
							{game.regionNew["Animal Kingdom"].keys.map(key => {
								{
									let roundIndicator = key - game.roundMeta[2].bracketIdMultiplier;
									if (roundIndicator < 100 && roundIndicator > 0) { 
										return <NewMatchup bracketId={key} />
									}
								}
							})}			
					</div>		
					<div class="champion">
		<div class="semis-l current">
			<div class="round-details">west semifinals <br/></div>		
			<NewMatchup bracketId={game.regionNew["Championship"].keys[0]} />
			
		</div>
		<div class="final current">
			<i class="fa fa-trophy"></i>
			<div class="round-details">championship <br/></div>		
			<NewMatchup bracketId={game.regionNew["Championship"].keys[2]} />
		</div>
		<div class="semis-r current">		
			<div class="round-details">east semifinals <br/></div>	
			<NewMatchup bracketId={game.regionNew["Championship"].keys[1]} />
		</div>	
	</div>

					<div class="split split-two">


<div class="round round-three current">
	<div class="round-details">Round 3<br/></div>						
	{
	game.regionNew["Epcot"].keys.map(key => {
			{
				let roundIndicator = key - game.roundMeta[2].bracketIdMultiplier;
				if (roundIndicator < 100 && roundIndicator > 0) { 
					return <NewMatchup bracketId={key} />
				}
			}
		})}
		
		{game.regionNew["Hollywood Studios"].keys.map(key => {
			{
				let roundIndicator = key - game.roundMeta[2].bracketIdMultiplier;
				if (roundIndicator < 100 && roundIndicator > 0) { 
					return <NewMatchup bracketId={key} />
				}
			}
		})}											
</div>		

<div class="round round-two current">
	<div class="round-details">Round 2<br/></div>						
		{game.regionNew["Epcot"].keys.map(key => {
			{
				let roundIndicator = key - game.roundMeta[1].bracketIdMultiplier;
				if (roundIndicator < 100 && roundIndicator > 0) { 
					return <NewMatchup bracketId={key} />
				}
			}
		})}
		
		{game.regionNew["Hollywood Studios"].keys.map(key => {
			{
				let roundIndicator = key - game.roundMeta[1].bracketIdMultiplier;
				if (roundIndicator < 100 && roundIndicator > 0) { 
					return <NewMatchup bracketId={key} />
				}
			}
		})}								
</div>

<div class="round round-one current">
			<div class="round-details">Round 1<br/></div>
				{game.regionNew["Epcot"].keys.map(key => {
								{
									let roundIndicator = key - game.roundMeta[0].bracketIdMultiplier;
									if (roundIndicator < 100 && roundIndicator > 0) { 
										return <NewMatchup bracketId={key} />
									}
								}
							})}
							
							{game.regionNew["Hollywood Studios"].keys.map(key => {
								{
									let roundIndicator = key - game.roundMeta[0].bracketIdMultiplier;
									if (roundIndicator < 100 && roundIndicator > 0) { 
										return <NewMatchup bracketId={key} />
									}
								}
							})}
			
								
		</div>	     
</div>
				</div>
			</section>
        </React.Fragment>
    )
}

export default appAlt;
