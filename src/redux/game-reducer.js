import { LOAD_GAME, CHOOSE_WINNER, NEW_CHOOSE_WINNER } from '../actions';



const REGION_METADATA =[
  {"name": "Magic Kingdom", "position": "TL", "sectionId": 1},
  {"name":"Hollywood Studios", "position": "BL", "sectionId": 2},
  {"name":"Epcot" ,"position": "TR", "sectionId": 3},
  {"name":"Animal Kingdom", "position":"BR", "sectionId": 4},
  {"name":"Championship",  "sectionId": 5}
]

const ROUND_ONE_RANGE = [[1,2,3,4],[5,6,7,8], [9,10,11,12], [13,14,15,16]]

const ROUND_METADATA =[
  {
    "title" : "Round One",
    "bracketIdMultiplier": 100,
    "isRegion" : true,
    "isChampionship": false
  },
  {
    "title" : "Sweet 16",
    "bracketIdMultiplier": 200,
    "isRegion" : true,
    "isChampionship": false
  },
  {
    "title" : "Elite 8",
    "bracketIdMultiplier": 300,
    "isRegion" : true,
    "isChampionship": false
  },
  {
    "title" : "Final 4",
    "bracketIdMultiplier": 400,
    "isRegion" : false,
    "isChampionship": false
  },
  {
    "title" : "Championship",
    "bracketIdMultiplier": 500,
    "isRegion" : false,
    "isChampionship": true
  }
]



const REGION_METADATA =[
  {"name": "Magic Kingdom", "position": "TL", "sectionId": 1},
  {"name":"Hollywood Studios", "position": "BL", "sectionId": 2},
  {"name":"Epcot" ,"position": "TR", "sectionId": 3},
  {"name":"Animal Kingdom", "position":"BR", "sectionId": 4},
  {"name":"Championship",  "sectionId": 5}
]

const ROUND_ONE_RANGE = [[1,2,3,4],[5,6,7,8], [9,10,11,12], [13,14,15,16]]

const ROUND_METADATA =[
  {
    "title" : "Round One",
    "bracketIdMultiplier": 100,
    "isRegion" : true,
    "isChampionship": false
  },
  {
    "title" : "Sweet 16",
    "bracketIdMultiplier": 200,
    "isRegion" : true,
    "isChampionship": false
  },
  {
    "title" : "Elite 8",
    "bracketIdMultiplier": 300,
    "isRegion" : true,
    "isChampionship": false
  },
  {
    "title" : "Final 4",
    "bracketIdMultiplier": 400,
    "isRegion" : false,
    "isChampionship": false
  },
  {
    "title" : "Championship",
    "bracketIdMultiplier": 500,
    "isRegion" : false,
    "isChampionship": true
  }
]

const INITIAL_STATE = {
  regions: {},
  brackets: {},
  regionMeta: [...REGION_METADATA],
  roundMeta: [...ROUND_METADATA],
  regionNew: {}
}

export default function(state=INITIAL_STATE, action={payload: {}}){
  switch(action.type){
    case LOAD_GAME:
      let brackets = buildGames(action.payload);
      return {...state, regionNew: brackets.regionNew, ...flattenObject(brackets.brackets)}
    case CHOOSE_WINNER:
      let updatedBrackets = selectWinner(state, action.payload)

      return {...state, ...updatedBrackets};
    case NEW_CHOOSE_WINNER:
      selectWinner(state, action.payload)
      return state;
    default:
      return state;
  }
}

const flattenObject = (obj) => {
  const flattened = {}

  Object.keys(obj).forEach((key) => {
    flattened[key] = obj[key]
  })

  return flattened
}

function resetFutureBrackets(state, victorId, updates, isTop, losingTeam) {

  if(victorId) {

    let indexToReset = isTop ? 0 : 1
    let possibleUpdate = state[victorId]
    console.log(losingTeam, victorId, possibleUpdate)
    if(possibleUpdate.teams[indexToReset].name === losingTeam.name)
    {
      possibleUpdate.teams[indexToReset] = {name: "TBD", type:"placeholder"}
    }
    updates.push(possibleUpdate);
    resetFutureBrackets(state, possibleUpdate.victorBracket, updates, possibleUpdate.isTop, losingTeam);
  }

  return updates;

}


function selectWinner(state, payload) {
  console.log(payload);

  let currBracket = state[payload.currBracketId];
  let victorBracket = payload.victorId && state[payload.victorId];
  let team = payload.team;
  let losingTeam = currBracket.teams.find(teamInstance => {return teamInstance.name !== team.name});

  console.log(losingTeam)

  currBracket.winner = team;

  let indexOfWinner = currBracket.isTop ? 0 : 1
  
  if(victorBracket) {
    victorBracket.teams[indexOfWinner] = team;
    resetFutureBrackets(state, victorBracket.victorBracket, [], victorBracket.isTop, losingTeam);
  }

  return [currBracket, victorBracket];
}

function buildGames(regions) {
  let brackets = {};

  let gameBoard = buildFutureRounds(32);
  Object.assign(brackets, gameBoard.brackets);

  populateBrackets(gameBoard.brackets, regions);


  return {brackets, regionNew: gameBoard.regionNew};
}

function populateBrackets(brackets, regions) {

  let round = {
    ...ROUND_METADATA[0]
  }


  regions.forEach((region,idx) => {

    let regionMeta = {
      ...REGION_METADATA[idx]
    }
     // this is used to ensure seed 1 v 8, 2 v 7...etc
      const lastIndex = region.teams.length -1;

      // 2 teams in a match... total number of teams / 2 will give us total matches in round 1
      const totalMatches = region.teams.length / 2;
      let id = round.bracketIdMultiplier + ((regionMeta.sectionId * 4) - 3);
   
      region.teams.forEach((team,teamIdx) => {
        
        if (!(totalMatches - (teamIdx+1) >=0)) {return;}
  
        brackets[id+teamIdx].teams = [
          {name: team, type: "team"},
          {name: region.teams[lastIndex - teamIdx], type: "team"}
        ]
    });

  });

}

function buildFutureRounds(numberOfRemainingTeams) {

  const remainingRounds = Math.log2(numberOfRemainingTeams);
  let brackets = {};
  let regionNew = {};

  let roundMatches = numberOfRemainingTeams / 2;

  // Create each round
  for(let i=0; i <= remainingRounds; i++) {

    let round = {
      ...ROUND_METADATA[i]
    }

    let idMultiplier = (i+1) * 100;
    //within each round, create the games

    let top = true;

    for (let j = 1; j <= roundMatches; j++ ) {
      let id= idMultiplier + j;
      let region = {};
      
      if(roundMatches === 4) {
        region = REGION_METADATA[j-1]
      } else if(roundMatches === 16) {
        ROUND_ONE_RANGE.forEach((range,idx) => {
          if(range.includes(j)) {
             region = REGION_METADATA[idx]
          }
        })
      } 
      else {
        region = round.isRegion ? REGION_METADATA[(Math.round(j/2)) -1] : REGION_METADATA[4];
      }

      brackets[id] = {
        "id": id,
        "teams": [
          {name: "TBD", type: "team"},
          {name: "TBD", type: "team"}
        ],
        round,
        region: region,
        isTop: top,
        "victorBracket": !round.isChampionship && Math.round((id-idMultiplier)/2) + 100 + idMultiplier
      }

      if(!regionNew [region.name]) {
        regionNew[region.name] = {
          name: region.name,
          keys: [id]
        }
      } else {
        regionNew[region.name].keys.push(id);
      }
      top = !top;
    }
    roundMatches = roundMatches / 2; 
  }
  return {brackets, regionNew};
}