import React from 'react';
import '../styles.css';
import {useDispatch, useSelector, shallowEqual} from 'react-redux';
import { newChooseWinner} from '../actions';



const classes = {
    0: "team-top",
    1: "team-bottom"
}


const NewMatchup = ({bracketId, championship}) => {

    const dispatch = useDispatch();
    const bracket = useSelector(state => state.game[bracketId])

    const teamName1 = useSelector(state => state.game[bracketId].teams[0]);
    const teamName2 = useSelector(state => state.game[bracketId].teams[1]);

    const teams = [teamName1, teamName2]

    const globalClassName = championship ? "matchup championship" : "matchup";

    return (
        <ul className={globalClassName}>
            {
                teams.map((team, idx) => {
                    return <li key={idx} 
                    onClick={()=> dispatch(newChooseWinner({team, currBracketId: bracket.id, victorId: bracket.victorBracket}))} 
                    className={"team " + classes[idx]}>{team.name}</li>
            })}
            
           
		</ul>
    );
};

export default NewMatchup;
