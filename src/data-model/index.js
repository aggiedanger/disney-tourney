export const newInput = [
    {
        name: "Magic Kingdom",
        teams: [
            "Seven Dwarfs Mine Train",
            "Space Mountain",
            "Big Thunder Mountain",
            "Splash Mountain",
            "Peter Pan",
            "Haunted Mansion",
            "Buzz Lightyear",
            "Pirates of the Caribbean"            
        ]
    },
    {
        name: "Hollywood Studios",
        teams: [
            "Rise of the Resistance",
            "Millenium Falcon: Smugglers Run",
            "Mickey and Minnie's Runaway Railway",
            "Slinky Dog Dash",
            "Tower of Terror",
            "Rock n Rollcercoaster",
            "Toy Story Mania",
            "Alien Swirling Saucers"            
        ]
    },
    {
        name: "Epcot",
        teams: [
            "Frozen Ever After",
            "Test Track",
            "Soarin'",
            "Living with the Land",
            "Finding Nemo",
            "Spaceship Earth",
            "Mission: Space",
            "Gran Fiesta Tour"            
        ]
    },
    {
        name: "Animal Kingdom",
        teams: [
            "Flight of Passage",
            "Expedition Everest",
            "Kilimanjaro Safari",
            "Dinosaur",
            "Navi River Journey",
            "Kali River Rapids",
            "TriceraTop Spin",
            "Wildlife Express Train"            
        ]
    }
]