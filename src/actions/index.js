export const LOAD_GAME = "LOAD_GAME";
export const CHOOSE_WINNER = "CHOOSE_WINNER";
export const NEW_CHOOSE_WINNER = "NEW_CHOOSE_WINNER";

export function loadGame(config){
  return{
    type: LOAD_GAME,
    payload: config
  }
}

export function chooseWinner(matchup){
  return {
    type: CHOOSE_WINNER,
    payload: matchup
  }
}

export function newChooseWinner(matchup){
  return {
    type: NEW_CHOOSE_WINNER,
    payload: matchup
  }
}